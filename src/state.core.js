sap.ui.define(["./require", "sap/ui/model/json/JSONModel"], function(
	require,
	JSONModel
) {
	"use strict";

	var _ = require("lodash");
	var Rx = require("rxjs");

	var stateKey = "__state__";
	var actionsKey = "__actions__";
	var gettersKey = "__getters__";
	var moduleId = "__hash__";
	var stateTree = {};
	var originalStates = {};
	var slashRegExp = /\//g;

	var commonModule = {
		state: {
			loading: false
		},
		actions: {
			setLoading: function(context, value) {
				return Rx.of({
					loading: value
				});
			},
			setData: function(context, data) {
				return Rx.of(data);
			}
		}
	};

	function uuidv4() {
		return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(
			c
		) {
			var r = (Math.random() * 16) | 0,
				v = c == "x" ? r : (r & 0x3) | 0x8;
			return v.toString(16);
		});
	}

	function mutate(data) {
		_.mergeWith(stateTree[this[moduleId]][stateKey], data, function(
			dest,
			src
		) {
			if (Array.isArray(dest)) {
				return [...src];
			}
		});

		for (var k of Object.keys(
			stateTree[this[moduleId]][gettersKey] || {}
		)) {
			stateTree[this[moduleId]][stateKey][k] = stateTree[this[moduleId]][
				gettersKey
			][k](stateTree[this[moduleId]][stateKey]);
		}
		if (this.jsonModel) {
			this.jsonModel.refresh();
		}
		return stateTree[this[moduleId]][stateKey];
	}

	function findState(path, statesTree) {
		if ((path.match(slashRegExp) || []).length <= 1) {
			return statesTree[path.replace(slashRegExp, "")];
		}
		var state = findState(path.slice(0, path.lastIndexOf("/")), statesTree);

		return state[
			path.slice(path.lastIndexOf("/")).replace(slashRegExp, "")
		];
	}

	function resetBranch(path, targetBranch, defaultState) {
		if (path === "/" || !path.match(slashRegExp)) {
			return _.cloneDeep(defaultState);
		} else {
			var nextSlash = path.slice(1).indexOf("/") + 1;
			var key =
				nextSlash === 0
					? path.substr(1)
					: path.substr(1, nextSlash - 1);

			if (targetBranch[stateKey]) {
				targetBranch = targetBranch[stateKey];
			}
			if (typeof targetBranch[key] === "object") {
				return Object.assign(
					targetBranch[key],
					resetBranch(
						path.slice(key.length + 1),
						targetBranch[key],
						defaultState
					)
				);
			} else {
				targetBranch[key] = resetBranch(
					path.slice(key.length + 1),
					targetBranch[key],
					defaultState
				);
			}
		}
	}

	function dateOrArray(obj) {
		return Array.isArray(obj) || _.isDate(obj);
	}

	function buildPatch(tree) {
		var patch = {};

		for (var b of Object.keys(tree)) {
			var nextSlashIndex = b.slice(1).indexOf("/");

			if (
				tree[b] &&
				typeof tree[b] === "object" &&
				!dateOrArray(tree[b])
			) {
				tree[b] = buildPatch(tree[b]);
			}
			if (nextSlashIndex > 0) {
				var aux = {
					[b.slice(nextSlashIndex + 1)]: tree[b]
				};

				patch[b.substr(1, b.slice(1).indexOf("/"))] = buildPatch(aux);
			} else {
				patch[b.slice(b.indexOf("/") < 0 ? 0 : 1)] = tree[b];
			}
		}
		return patch;
	}

	function BBState(module) {
		this[moduleId] = uuidv4();
		_.merge(module.state, commonModule.state);
		_.merge(module.actions, commonModule.actions);
		stateTree = {
			...stateTree,
			[this[moduleId]]: {
				[stateKey]: _.cloneDeep(module.state),
				[actionsKey]: _.mapValues(module.actions, function(fn) {
					return fn.bind(this);
				}),
				[gettersKey]: module.getters
			}
		};
		originalStates[this[moduleId]] = _.cloneDeep(module.state);

		for (var k of Object.keys(module.getters || {}) || []) {
			stateTree[this[moduleId]][stateKey][k] = null;
		}

		this.select = function(path) {
			return findState(path, stateTree[this[moduleId]][stateKey]);
		};

		this.dispatch = function(action, payload) {
			var self = this;

			return stateTree[this[moduleId]][actionsKey][action](
				{
					dispatch: this.dispatch.bind(this),
					state: Object.assign(
						{},
						stateTree[this[moduleId]][stateKey]
					),
					_controller: this.controllerContext
				},
				payload
			).pipe(
				Rx.operators.switchMap(function(data) {
					if (data && typeof data === "object") {
						return Rx.of(mutate.call(self, buildPatch(data)));
					}
					return of(true);
				})
			);
		};

		this.syncJSONModel = function(view, modelName, mode, controllerCtx) {
			this.controllerContext = controllerCtx || {};
			if (this.jsonModel) {
				return this.jsonModel;
			}
			var model = new JSONModel(
				stateTree[this[moduleId]][stateKey],
				true
			);

			model.setDefaultBindingMode(mode || "TwoWay");
			view.setModel(model, modelName);
			this.jsonModel = model;
			return model;
		};

		this.reset = function(path) {
			// TODO: Set as built-in action
			path = `/${this[moduleId]}${path}`;
			resetBranch(path, stateTree, findState(path, originalStates));

			if (this.jsonModel) {
				this.jsonModel.refresh();
			}
		};
	}

	return {
		BBState: BBState
	};
});

# ui5_store

Prototype of a library based on RxJs to manage sapui5 apps state in a predictable way. Based on Flux pattern and Vuex library.
This library uses JSONModel to store the state.

This library allows developers move app state management to diferent module and decouple
state management of app business logic. 

Key concepts are.

**Actions**. Dispatched from controllers, services and other actions to perform operations such as
Data access, API requests and state mutations. Every action returns and Observable that carries
a patch of the state, then the library updates the current state to a new one using that
patch. (currentState + statePatch = newState)

**Getters**. Used to compute complex variables from state.

# Summary

This library is intended to decouple sapui5 apps.